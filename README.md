# Ondergrond

Collect data about underground infrastructure. Enable a quick overview of the possible 
underground infrastructe options available at a location.

# Stappenplan landelijke complexiteitskaart (opzet TTE engineering)

Dit het is het stappenplan dat gevolgd is om een ondergrondse `complexiteit` - kaart proof of concept 
te maken voor Hoorn. We hebben de ambitie om de flow te 'verdockeren'.

## Data verzamelen

|  Databron asset type |  Bron Download data / opslaan van data via WFS |
| :--------------------| :----------------------------------------------|
| Buisleidingen | http://servicespub.risicokaart.nl/rk_services_pub/servi-ces/WFS-risicokaart |
| Gas - Hoofdtransportleidingnet | https://www.risicokaart.nl/welke-risicos-zijn-er/risico-transport-gevaarlijke-stoffen/buisleidingen |
| Gas Regionale leidingen hoge druk | Wachten op Vivet | 
| Gas - Regionale leidingen lage druk | Wachten Vivet  |
| Electra - Nationaal hoogspanningsnet |  (Tennet) - Mast https://www.arcgis.com/home/item.html?id=646a6dee22bf485587bc4daf98da1306
| Electra - Nationaal hoogspanningsnet |  (Tennet) - Hoogspanning leiding (bovengronds) https://www.arcgis.com/home/item.html?id=646a6dee22bf485587bc4daf98da1306 |
| Electra - Nationaal hoogspanningsnet | (Tennet) - Hoogspanning kabel (ondergronds) https://www.arcgis.com/home/item.html?id=646a6dee22bf485587bc4daf98da1306 |
| Electra - Regionale leidingen hoog - spanning | wachten op Vivet |
| Electra - Regionale leidingen midden - spanning | wachten op Vivet |
| Electra - Regionale leidingen laag - spanning | wachten op Vivet |
| Riolering - put | https://www.pdok.nl/introductie/-/article/stedelijk-water-riolering- |
| Riolering - streng | https://www.pdok.nl/introductie/-/article/stedelijk-water-riolering-  |
| Topo - Wijken / Buurten | https://geodata.nationaalgeoregister.nl/wijkenbuur-ten2019/wfs |
| Topo - Gebouwen | https://geodata.nationaalgeoregister.nl/bag/wfs/v1_1 |
| Infra - Bruggen | https://app.pdok.nl/lv/bgt/download-viewer/ |
| Infra - Dijken | https://app.pdok.nl/lv/bgt/download-viewer/  |
| Infra - Provinciale wegen / snelwegen | https://app.pdok.nl/lv/bgt/download-viewer/ |
| Infra - Spoorlijnen | https://app.pdok.nl/lv/bgt/download-viewer/ |
| Infra - Tunnels  | https://app.pdok.nl/lv/bgt/download-viewer/ |
| Infra - Viaducten | https://app.pdok.nl/lv/bgt/download-viewer/ |
| Infra - Waterkeringen | https://app.pdok.nl/lv/bgt/download-viewer/ |

## Data klaar maken

1. Zo nodig data omzetten naar shapefile formaat.

- BGT

2. Filteren op relevante informatie

- BGT: alle niet relevante objecten er uit. Dus per laag kijken of er relevante informatie in zit, en de rest er uit gooien. We hebben een overzicht gemaakt van de info van Hoorn, maar dat is niet gelijk aan heel Nederland.

Signaleringskaart klaarmaken

  -  Kleuren toewijzen

Data klaarmaken voor complexiteitskaart

  -  Databron via QGIS naar de database slepen. Benaming: `Gemeente_databron`vb. `Hoorn_Buisleidingen`

Buffer meegeven

-  Materialised view van de databron aanmaken met buffer (zie tabel 2)

```sql
SELECT "Hoorn_Buisleidingen".id,

st_buffer("Hoorn_Buisleidingen".geom, 50::double precision) AS st_buffer

FROM "data_C20013"."Hoorn_Buisleidingen";
```

Benaming: `vmg_Gemeente_databron_buffer` vb `vmg_Hoorn_Buisleidingen_buffer`

# Databron Buffer toevoegen aan databronnen in m

| Bron | Buffer in meters |
|:-----|:-----------------|
| Buisleidingen | 50      |
| Gas - Hoofdtransportleidingnet | 50 |
| Gas - Regionale leidingen hoge druk | 1 |
| Gas - Regionale leidingen lage druk | 1 |
| Electra - Nationaal hoogspanningsnet - Mast | 3 bij punt locatie |
| Electra - Nationaal hoogspanningsnet - Hoogspanning leiding (bovengronds) | 0,5 |
| Electra - Nationaal hoogspanningsnet - Hoogspanning kabel (ondergronds) | 1 | 
| Electra - Regionale leidingen hoogspanning | 0,5 |
| Electra - Regionale leidingen middenspanning  | 0,5 |
| Electra - Regionale leidingen laagspanning | 0,5 |
| Riolering - put | 1 |
| Riolering - streng | 0,5 |
| Topo - Gebouwen | 0 |
| Infra - Bruggen | 0 |
| Infra - Dijken 5 |  
| Infra - Provinciale wegen / snelwegen | 0 |
| Infra - Spoorlijnen|   8 |
| Infra - Tunnels | 0 |
| Infra - Viaducten | 0 |
| Infra - Waterkeringen | 5 |


# Het maken en samenvoegen van geometrien.

3. Uitbreiden Hoorn_singlegeom_buffer:

```sql
Alter table hoorn_singlegeom_buffer add column Buisleidingen geometry
```

4. Updaten table Hoorn_singlegeom_buffer:

```sql
UPDATE "data_C20013"."Hoorn_singlegeom_buffer" SET ("Buisleidingen") =

(SELECT st_union(st_buffer) FROM "data_C20013"."Hoorn_Buisleidingen”);
```

Relevante bronnen uit buurtoppervlak knippen

5. Uitbreiden code in vmg_Hoorn_buurten_ckaart

```sql
SELECT b.id,

b.water,

b.opp AS "Oppervlak buurt [m2]",

1::double precision - st_area(st_difference(st_difference(st_diffe-rence(st_difference(st_difference(st_difference(st_difference(st_diffe-rence(st_difference(st_difference(st_difference(st_difference(st_diffe-rence(st_difference(st_difference(st_difference(b.geom, g."Bomen"), g."Buisleidingen"), g."TenneT-Hoogspanningsleiding"), g."TenneT-Hoog-spanningskabel"), g."Gas hoge druk"), g."Gas lage druk"), g."Middenspan-ningskabels"), g."Panden"), g.spoorwegtrace), g.bgt_tunneldeel), g.hoog-spanningskabels), g.laagspanningskabels), g.riool_put), g.laagspannings-verdeelkasten), g.middenspanningsinstallaties), g.elektra_onderstations)) / b.opp::double precision AS "Oppervlak gebruikt [%]"

FROM "data_C20013_Complexiteitskaart"."Hoorn_CBS buurten" b,

"data_C20013_Complexiteitskaart"."Hoorn_singlegeom_buffer" g

WHERE b.gemeentena::text = 'Hoorn'::text

ORDER BY b.id;
```

6. Refreshen materialized views

```sql
vmg_Gemeente_databron_buffer refresh with data

vmg_Hoorn_buurten_ckaart refresh with data
```
